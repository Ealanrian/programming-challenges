#include <stdio.h>

void main(void)
{
	int i;
	int print =0;
	for (i =0; i<100; i++)
	{
		if( (i%3)==0 )
		{
			printf("Fizz");
			print =1;
		}
		if( (i%5)==0 )
		{
			if(print ==1)
			{
				printf(" ");
			}
			printf("Buzz");
			print = 1;
		}
		if( print ==0 )
		{
			printf("%d", i);
		}
		printf(", ");
		print= 0;
	}	
}
